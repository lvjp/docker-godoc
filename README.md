# docker-godoc

[![GitLab CI](https://gitlab.com/lvjp/docker-godoc/badges/master/pipeline.svg)](https://gitlab.com/lvjp/docker-godoc/commits/master)
[![Licence](https://img.shields.io/badge/license-GPL--3.0-brightgreen)](https://gitlab.com/lvjp/docker-godoc/blob/master/COPYING.md)

This project package [godoc]()https://godoc.org/ inside a docker image ready
to use inside a GitLab CI pipeline.

Since [go1.13](https://golang.org/doc/go1.13#godoc), the godoc webserver is no longer
included in the main binary distribution. That why i built this image.

## License

Full license can be found in the [COPYING.md](COPYING.md) file.

        Copyright (C) 2019 VERDOÏA Laurent

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
