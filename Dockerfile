# vim:set ts=4 sw=4 tw=95 et:

FROM golang:1.13.4-buster

RUN go get -v golang.org/x/tools/cmd/godoc

ENTRYPOINT [ "/go/bin/godoc" ]
CMD [ "--help" ]
